package generators

import java.time.LocalDate

import flights.model.Flight
import org.scalacheck.Gen
import org.scalacheck.Prop.forAll
import org.scalacheck.Test.Parameters

object FlightsGeneratorDemo {
  private def buildDateGen(): Gen[LocalDate] = {
    val monthGen = Gen.oneOf(java.time.Month.values())
    val yearGen = Gen.choose(2019, 2024)
    for {
      month <- monthGen
      leapYear <- Gen.oneOf(Seq(true, false))
      day <- Gen.choose(1, month.length(leapYear))
      year <- if (!leapYear) yearGen else Gen.const(2020)
    } yield LocalDate.of(year, month.getValue, day)
  }

  private def buildFlightGen(): Gen[Flight] = {
    val airports = Seq(
      "London", "Belfast", "Boston",
      "New York", "Chicago", "Paris",
      "Madrid", "Toronto", "Austin")
    for {
      number <- Gen.choose(100, 999)
      origin <- Gen.oneOf(airports)
      destination <- Gen.oneOf(airports.filterNot(_ == origin))
      departure <- buildDateGen()
      flightTime <- Gen.choose(0, 3)
      arrival <- Gen.const(departure.plusDays(flightTime))
    } yield new Flight(number, origin, destination, departure, arrival)
  }

  def main(args: Array[String]) {
    val prop = forAll(buildFlightGen()) { flight =>
      println(flight)
      true
    }
    val params = Parameters.default.withMinSuccessfulTests(100)
    prop.check(params)
  }
}
