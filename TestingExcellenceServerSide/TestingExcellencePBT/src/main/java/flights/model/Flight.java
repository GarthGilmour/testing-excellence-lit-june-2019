package flights.model;

import java.time.LocalDate;
import java.util.Objects;

public class Flight {
    private int number;
    private String origin;
    private String destination;
    private LocalDate departure;
    private LocalDate arrival;

    public Flight(int number, String origin, String destination, LocalDate departure, LocalDate arrival) {
        this.number = number;
        this.origin = origin;
        this.destination = destination;
        this.departure = departure;
        this.arrival = arrival;
    }

    public Flight() {
    }

    @Override
    public String toString() {
        String msg = "Flight %s from %s to %s leaving at %tF and arriving at %tF";
        return String.format(msg, number, origin, destination, departure, arrival);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flight flight = (Flight) o;
        return number == flight.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public LocalDate getDeparture() {
        return departure;
    }

    public void setDeparture(LocalDate departure) {
        this.departure = departure;
    }

    public LocalDate getArrival() {
        return arrival;
    }

    public void setArrival(LocalDate arrival) {
        this.arrival = arrival;
    }
}

