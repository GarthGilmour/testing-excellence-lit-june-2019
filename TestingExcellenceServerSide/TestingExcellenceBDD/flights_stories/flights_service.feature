Feature: Working with the Flights service

  Scenario: There are 20 flights by default
    Given the flights service
    When we request all flights
    Then there are 20 flights

  Scenario: Flights can be found by origin
    Given the flights service
    When we request flights leaving from Dublin
    Then there are 10 flights
    And all flights originate from Dublin

  Scenario: Flights can be found by destination
    Given the flights service
    When we request flights heading to Birmingham
    Then there are 5 flights
    And all flights are heading to Birmingham

  Scenario: Flights can be found by id
    Given the flights service
    When we request flight 203
    Then the following flight is returned
      | number      | 203          |
      | origin      | Belfast City |
      | destination | Birmingham   |

  Scenario: Flights can be added
    Given the flights service
    When we add the following flight
      | number      | 901        |
      | origin      | Atlantis   |
      | destination | Shangri-La |
    And we request all flights
    Then there are 21 flights
    And we request flight 901
    Then the following flight is returned
      | number      | 901        |
      | origin      | Atlantis   |
      | destination | Shangri-La |

  Scenario: Flights can be removed
    Given the flights service
    When we remove flight 901
    And we request all flights
    Then there are 20 flights
    And flight 901 does not exist