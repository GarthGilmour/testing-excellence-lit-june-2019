package testing.excellence.bdd;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java8.En;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Map;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.junit.Assert.assertEquals;

public class FlightsStepDefinitions implements En {
    private static final String BASIC_SERVICE_URL = "http://localhost:8080/flights";
    private WebTarget target;
    private Flight[] flights;

    public FlightsStepDefinitions() {
        Given("^the flights service$", () -> {
            Client client = ClientBuilder.newClient();
            client.register(new ObjectMapperContextResolver());
            target = client.target(BASIC_SERVICE_URL);
        });
        When("^we request all flights$", () -> {
            flights = target.request(APPLICATION_JSON)
                    .get(Flight[].class);
        });
        When("^we request flights leaving from ([a-zA-Z ]+)$", (String airport) -> {
            flights = target.path("origin")
                    .path(airport)
                    .request(APPLICATION_JSON)
                    .get(Flight[].class);
        });
        When("^we request flight (\\d+)$", (Integer id) -> {
            Flight result = target.path(String.valueOf(id))
                    .request(APPLICATION_JSON)
                    .get(Flight.class);
            flights = new Flight[]{result};
        });
        When("^we request flights heading to ([a-zA-Z ]+)$", (String airport) -> {
            flights = target.path("destination")
                    .path(airport)
                    .request(APPLICATION_JSON)
                    .get(Flight[].class);
        });
        When("^we remove flight (\\d+)$", (Integer id) -> {
            Response response = target.path(String.valueOf(id))
                    .request(APPLICATION_JSON)
                    .delete();
            assertEquals(200, response.getStatus());
        });
        When("^we add the following flight$", (DataTable table) -> {
            Map<String, String> flightData = table.asMap(String.class, String.class);
            Flight flight = new Flight(Integer.parseInt(flightData.get("number")),
                    flightData.get("origin"),
                    flightData.get("destination"),
                    LocalDate.now(),
                    LocalDate.now());
            Entity<Flight> entity = Entity.entity(flight, APPLICATION_JSON);
            Response response = target.request(APPLICATION_JSON)
                    .put(entity);
            assertEquals(201, response.getStatus());
        });
        Then("^there are (\\d+) flights$", (Integer number) -> {
            assertEquals(number.intValue(), flights.length);
        });
        Then("^all flights originate from ([a-zA-Z ]+)$", (String airport) -> {
            Arrays.stream(flights)
                    .forEach(f -> assertEquals(airport, f.getOrigin()));
        });
        Then("^all flights are heading to ([a-zA-Z ]+)$", (String airport) -> {
            Arrays.stream(flights)
                    .forEach(f -> assertEquals(airport, f.getDestination()));
        });
        Then("^the following flight is returned$", (DataTable table) -> {
            Map<String, String> flightData = table.asMap(String.class, String.class);
            Flight result = flights[0];
            assertEquals(Integer.parseInt(flightData.get("number")), result.getNumber());
            assertEquals(flightData.get("origin"), result.getOrigin());
            assertEquals(flightData.get("destination"), result.getDestination());
        });
        Then("^flight (\\d+) does not exist$", (Integer id) -> {
            Response response = target.path(String.valueOf(id))
                    .request(APPLICATION_JSON)
                    .get();
            assertEquals(404, response.getStatus());
        });
    }
}
