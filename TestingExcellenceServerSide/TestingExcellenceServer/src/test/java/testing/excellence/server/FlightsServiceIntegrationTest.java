package testing.excellence.server;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class FlightsServiceIntegrationTest {
    private static final MediaType JSON_CONTENT_TYPE = MediaType.parseMediaType("application/json;charset=UTF-8");

    @Autowired
    private MockMvc mockMvc;

	@Test
	public void shouldFindAllFlights() throws Exception {
        mockMvc.perform(get("/flights").accept(JSON_CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(20)))
                .andExpect(jsonPath("$[?(@.number =~ /1[0-9][0-9]/)]", hasSize(5)))
                .andExpect(jsonPath("$[?(@.number =~ /2[0-9][0-9]/)]", hasSize(5)))
                .andExpect(jsonPath("$[?(@.number =~ /3[0-9][0-9]/)]", hasSize(5)))
                .andExpect(jsonPath("$[?(@.number =~ /4[0-9][0-9]/)]", hasSize(5)));
	}

    @Test
    public void shouldFindFlightsByOrigin() throws Exception {
        mockMvc.perform(get("/flights/origin/Dublin").accept(JSON_CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(10)))
                .andExpect(jsonPath("$[?(@.origin == 'Dublin')]", hasSize(10)));
    }

    @Test
    public void shouldFindFlightsByDestination() throws Exception {
        mockMvc.perform(get("/flights/destination/Birmingham").accept(JSON_CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(5)))
                .andExpect(jsonPath("$[?(@.destination == 'Birmingham')]", hasSize(5)));
    }

    @Test
    public void shouldFindFlightsById() throws Exception {
        mockMvc.perform(get("/flights/203").accept(JSON_CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.number").value("203"))
                .andExpect(jsonPath("$.origin").value("Belfast City"))
                .andExpect(jsonPath("$.destination").value("Birmingham"));
    }

    @Test
    @DirtiesContext
    public void shouldDeleteFlightsById() throws Exception {
        mockMvc.perform(delete("/flights/203"))
                .andExpect(status().isOk());

        //after deletion only 19 flights should remain
        mockMvc.perform(get("/flights").accept(JSON_CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(19)))
                .andExpect(jsonPath("$[?(@.number=='203')]", hasSize(0)));

        //flight 203 should no longer exist
        mockMvc.perform(get("/flights/203").accept(JSON_CONTENT_TYPE))
                .andExpect(status().isNotFound());
    }

    @Test
    @DirtiesContext
    public void shouldAddFlights() throws Exception {
        String content = "{\"number\":901,\"origin\":\"Atlantis\",\"destination\":\"Shangri-La\",\"departure\":\"2018-05-12\",\"arrival\":\"2018-05-12\"}";


        mockMvc.perform(put("/flights").contentType(JSON_CONTENT_TYPE).content(content))
                .andExpect(status().isCreated());

        //after addition 21 flights should exist
        mockMvc.perform(get("/flights").accept(JSON_CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(21)))
                .andExpect(jsonPath("$[?(@.number=='901')]", hasSize(1)));

        //flight 901 should be findable
        mockMvc.perform(get("/flights/901").accept(JSON_CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.number").value("901"))
                .andExpect(jsonPath("$.origin").value("Atlantis"))
                .andExpect(jsonPath("$.destination").value("Shangri-La"));
    }

    @Test
    public void shouldFailToFetchViaUnknownId() throws Exception {
        mockMvc.perform(get("/flights/901").accept(JSON_CONTENT_TYPE))
                .andExpect(status().isNotFound());
    }

    @Test
    @DirtiesContext
    public void shouldFailToDeleteViaUnknownId() throws Exception {
        mockMvc.perform(get("/flights/901").accept(JSON_CONTENT_TYPE))
                .andExpect(status().isNotFound());
    }

    @Test
    @DirtiesContext
    public void shouldUpdateFlight() throws Exception {
        String content = "{\"number\":201,\"origin\":\"Atlantis\",\"destination\":\"Shangri-La\",\"departure\":\"2018-05-12\",\"arrival\":\"2018-05-12\"}";

        mockMvc.perform(put("/flights").contentType(JSON_CONTENT_TYPE).content(content))
                .andExpect(status().isOk());

        mockMvc.perform(get("/flights/201").accept(JSON_CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.number").value("201"))
                .andExpect(jsonPath("$.origin").value("Atlantis"))
                .andExpect(jsonPath("$.destination").value("Shangri-La"));

    }

}
