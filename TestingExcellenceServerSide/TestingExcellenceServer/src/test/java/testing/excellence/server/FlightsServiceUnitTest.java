package testing.excellence.server;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import testing.excellence.server.model.Flight;
import testing.excellence.server.repository.InMemoryFlightsRepository;
import testing.excellence.server.services.FlightsService;

import java.time.LocalDate;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class FlightsServiceUnitTest {
    private FlightsService service;

    @Before
    public void start() {
        service = new FlightsService(new InMemoryFlightsRepository());
    }

    @Test
    public void shouldFindAllFlights() {
        Set<Flight> results = service.allFlights();
        assertEquals(20, results.size());
        assertEquals(5, countFlightNumbersStartingWith(results, "1"));
        assertEquals(5, countFlightNumbersStartingWith(results, "2"));
        assertEquals(5, countFlightNumbersStartingWith(results, "3"));
        assertEquals(5, countFlightNumbersStartingWith(results, "4"));
    }

    @Test
    public void shouldFindFlightsByOrigin() {
        Set<Flight> results = service.flightsByOrigin("Dublin");
        assertEquals(10, results.size());
        results.forEach(f -> assertEquals("Dublin", f.getOrigin()));
    }

    @Test
    public void shouldFindFlightsByDestination() {
        Set<Flight> results = service.flightsByDestination("Birmingham");
        assertEquals(5, results.size());
        results.forEach(f -> assertEquals("Birmingham", f.getDestination()));
    }

    @Test
    public void shouldFindFlightsById() {
        ResponseEntity result = service.singleFlight(203);
        assertEquals(200, result.getStatusCodeValue());
        assertFlightEquals((Flight) result.getBody(), 203, "Belfast City", "Birmingham");
    }

    @Test
    public void shouldFailToFetchUnknownFlight() {
        ResponseEntity result = service.singleFlight(901);
        assertEquals(404, result.getStatusCodeValue());
    }

    @Test
    public void shouldDeleteFlights() {
        ResponseEntity deleteResult = service.deleteFlight(304);
        assertEquals(200, deleteResult.getStatusCodeValue());

        Set<Flight> fetchAllResult = service.allFlights();
        assertEquals(19, fetchAllResult.size());

        ResponseEntity fetchOneResult = service.singleFlight(304);
        assertEquals(404, fetchOneResult.getStatusCodeValue());
    }

    @Test
    public void shouldFailToDeleteUnknownFlight() {
        ResponseEntity result = service.singleFlight(901);
        assertEquals(404, result.getStatusCodeValue());
    }

    @Test
    public void shouldAddFlights() {
        Flight flight = new Flight(909, "Alantis", "Shangri-La", LocalDate.now(), LocalDate.now());
        ResponseEntity result = service.addOrUpdateFlight(flight);
        assertEquals(201, result.getStatusCodeValue());

        Set<Flight> fetchAllResult = service.allFlights();
        assertEquals(21, fetchAllResult.size());

        ResponseEntity fetchOneResult = service.singleFlight(flight.getNumber());
        assertEquals(200, fetchOneResult.getStatusCodeValue());
        assertFlightEquals((Flight) fetchOneResult.getBody(),
                flight.getNumber(),
                flight.getOrigin(),
                flight.getDestination());
    }

    @Test
    public void shouldUpdateFlight() {
        Flight flight = new Flight(401, "Alantis", "Olympus", LocalDate.now(), LocalDate.now());
        ResponseEntity result = service.addOrUpdateFlight(flight);
        assertEquals(200, result.getStatusCodeValue());

        ResponseEntity<Flight> response = service.singleFlight(401);
        assertEquals("Olympus", response.getBody().getDestination());

    }

    private long countFlightNumbersStartingWith(Set<Flight> input, String prefix) {
        return input.stream()
                .filter(f -> String.valueOf(f.getNumber()).startsWith(prefix))
                .count();
    }

    private void assertFlightEquals(Flight flight, int number, String origin, String destination) {
        assertEquals(number, flight.getNumber());
        assertEquals(origin, flight.getOrigin());
        assertEquals(destination, flight.getDestination());
    }
}
