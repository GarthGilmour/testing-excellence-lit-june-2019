package testing.excellence.server.repository;

import org.springframework.stereotype.Component;
import testing.excellence.server.model.Flight;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class InMemoryFlightsRepository implements FlightsRepository {
    private Set<Flight> schedule;

    public InMemoryFlightsRepository() {
        schedule = buildSchedule();
    }

    @Override
    public Set<Flight> all() {
        return schedule;
    }

    @Override
    public Set<Flight> byOrigin(String origin) {
        return schedule.stream()
                .filter(f -> f.getOrigin().equals(origin))
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Flight> byDestination(String destination) {
        return schedule.stream()
                .filter(f -> f.getDestination().equals(destination))
                .collect(Collectors.toSet());
    }

    @Override
    public Optional<Flight> byId(int id) {
        return schedule.stream()
                .filter(f -> f.getNumber() == id)
                .findFirst();
    }

    @Override
    public boolean addOrUpdate(Flight flight) {
        if (schedule.contains(flight)) {
            schedule.remove(flight);
            schedule.add(flight);
            return false;
        } else {
            schedule.add(flight);
            return true;
        }
    }

    @Override
    public boolean remove(int id) {
        Optional<Flight> found = schedule.stream()
                .filter(f -> f.getNumber() == id)
                .findFirst();
        return found.map(flight -> schedule.remove(flight))
                .orElse(false);
    }

    private Set<Flight> buildSchedule() {
        HashSet<Flight> schedule = new HashSet<>();
        schedule.add(new Flight(101, "Belfast City", "London Gatwick", buildDate(), buildDate()));
        schedule.add(new Flight(102, "Belfast City", "London Gatwick", buildDate(), buildDate()));
        schedule.add(new Flight(103, "Belfast City", "London Gatwick", buildDate(), buildDate()));
        schedule.add(new Flight(104, "Belfast City", "London Gatwick", buildDate(), buildDate()));
        schedule.add(new Flight(105, "Belfast City", "London Gatwick", buildDate(), buildDate()));
        schedule.add(new Flight(201, "Belfast City", "Birmingham", buildDate(), buildDate()));
        schedule.add(new Flight(202, "Belfast City", "Birmingham", buildDate(), buildDate()));
        schedule.add(new Flight(203, "Belfast City", "Birmingham", buildDate(), buildDate()));
        schedule.add(new Flight(204, "Belfast City", "Birmingham", buildDate(), buildDate()));
        schedule.add(new Flight(205, "Belfast City", "Birmingham", buildDate(), buildDate()));
        schedule.add(new Flight(301, "Dublin", "Edinburgh", buildDate(), buildDate()));
        schedule.add(new Flight(302, "Dublin", "Edinburgh", buildDate(), buildDate()));
        schedule.add(new Flight(303, "Dublin", "Edinburgh", buildDate(), buildDate()));
        schedule.add(new Flight(304, "Dublin", "Edinburgh", buildDate(), buildDate()));
        schedule.add(new Flight(305, "Dublin", "Edinburgh", buildDate(), buildDate()));
        schedule.add(new Flight(401, "Dublin", "Manchester", buildDate(), buildDate()));
        schedule.add(new Flight(402, "Dublin", "Manchester", buildDate(), buildDate()));
        schedule.add(new Flight(403, "Dublin", "Manchester", buildDate(), buildDate()));
        schedule.add(new Flight(404, "Dublin", "Manchester", buildDate(), buildDate()));
        schedule.add(new Flight(405, "Dublin", "Manchester", buildDate(), buildDate()));
        return schedule;
    }

    private LocalDate buildDate() {
        return LocalDate.now();
    }
}
