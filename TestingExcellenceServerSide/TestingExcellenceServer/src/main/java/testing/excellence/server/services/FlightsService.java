package testing.excellence.server.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import testing.excellence.server.model.Flight;
import testing.excellence.server.repository.FlightsRepository;

import java.util.Optional;
import java.util.Set;

import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/flights")
public class FlightsService {
    private FlightsRepository repository;

    @Autowired
    public FlightsService(FlightsRepository schedule) {
        this.repository = schedule;
    }

    @GetMapping(produces = "application/json")
    public Set<Flight> allFlights() {
        return repository.all();
    }

    @GetMapping(value = "/origin/{origin}", produces = "application/json")
    public Set<Flight> flightsByOrigin(@PathVariable("origin") String origin) {
        return repository.byOrigin(origin);
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Flight> singleFlight(@PathVariable("id") int id) {
        Optional<Flight> result = repository.byId(id);
        return result.map(flight -> new ResponseEntity<>(flight, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping(value = "/destination/{destination}", produces = "application/json")
    public Set<Flight> flightsByDestination(@PathVariable("destination") String destination) {
        return repository.byDestination(destination);
    }

    @DeleteMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<String> deleteFlight(@PathVariable("id") int id) {
        if (repository.remove(id)) {
            return ok("Success");
        }
        return notFound().build();
    }

    @PutMapping(consumes = "application/json")
    public ResponseEntity<String> addOrUpdateFlight(@RequestBody Flight flight) {
        if (repository.addOrUpdate(flight)) {
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return ok("Success");
    }
}
