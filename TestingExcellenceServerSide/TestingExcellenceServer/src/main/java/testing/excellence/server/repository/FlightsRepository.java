package testing.excellence.server.repository;

import testing.excellence.server.model.Flight;

import java.util.Set;
import java.util.Optional;

public interface FlightsRepository {
    Set<Flight> all();

    Set<Flight> byOrigin(String origin);

    Set<Flight> byDestination(String destination);

    Optional<Flight> byId(int id);

    boolean addOrUpdate(Flight flight);

    boolean remove(int id);
}
