package flights;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.client.WireMock;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;


public class Program {
    private static String FLIGHTS_URL = "/flights/";
    private static String SINGLE_FLIGHT_URL_REGEX = "/flights/([0-9]{3})";

    public static void main(String[] args) {
        WireMockServer server = new WireMockServer(
                options().usingFilesUnderDirectory("mappings")
        );

        server.stubFor(get(urlEqualTo(FLIGHTS_URL))
                .willReturn(buildResponseFromFile("sampleFlights.json")));

        server.stubFor(get(urlPathMatching(SINGLE_FLIGHT_URL_REGEX))
                .willReturn(buildResponseFromFile("singleFlight.json")));

        server.stubFor(delete(urlPathMatching(SINGLE_FLIGHT_URL_REGEX))
                .willReturn(buildDeleteResponse()));

        server.stubFor(put(urlPathMatching(FLIGHTS_URL))
                .willReturn(buildPutResponse()));

        server.stubFor(WireMock.options(urlPathMatching(SINGLE_FLIGHT_URL_REGEX))
                .willReturn(buildOptionsResponse()));

        server.stubFor(WireMock.options(urlPathMatching(FLIGHTS_URL))
                .willReturn(buildOptionsResponse()));

        server.start();
    }

    private static ResponseDefinitionBuilder buildOptionsResponse() {
        return addCorsHeaders(ok())
                .withHeader("Allow", "GET, HEAD, POST, PUT, DELETE, OPTIONS, PATCH");
    }

    private static ResponseDefinitionBuilder buildResponseFromFile(String filename) {
        return addCorsHeaders(ok())
                .withHeader("Content-Type", "application/json")
                .withBodyFile(filename);
    }

    private static ResponseDefinitionBuilder buildDeleteResponse() {
        return addCorsHeaders(ok());
    }

    private static ResponseDefinitionBuilder buildPutResponse() {
        return addCorsHeaders(ok())
                .withHeader("Content-Type", "application/json")
                .withBody("Success");
    }

    private static ResponseDefinitionBuilder addCorsHeaders(ResponseDefinitionBuilder builder) {
        builder.withHeader("Access-Control-Allow-Origin", "*")
                .withHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE")
                .withHeader("Access-Control-Max-Age", "3600")
                .withHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

        return builder;
    }
}
