# Materials for 'Testing Excellence' Delivery - 4th and 5th June 2019

## Introduction

This is a complex workshop, which explores unit, integration and system testing on both client and server via a range of frameworks. As such you may wish to perform the setup in advance. The sections below describe the projects within this repo and how to set them up. 

If you're in any doubt please don't worry - this can all be done at the start of the delivery and we have spare laptops available if required.

## Description of contents

This repo contains the following projects:

1. **TestingExcellenceServerSide/TestingExcellenceServer** - A simple RESTful service written using Spring Boot. This contains unit and integration tests, the latter written using Springs own component testing framework.
2. **TestingExcellenceServerSide/TestingExcellenceWireMock** - A dummy version of the above to show how RESTful services can quickly be stubbed out.
3. **TestingExcellenceServerSide/TestingExcellenceBDD** - Integration tests for the RESTful service written using Cucumber and the JAX-RS Client API (implemented in Jersey). Designed to show how the Gherkin syntax can be used as a 'lingua franca' by non developers.
4. **TestingExcellenceServerSide/TestingExcellence** - This project introduces Property Based Testing using the ScalaCheck library. It shows how we can create generator functions using for comprehensions / monads. These can then be included in integration tests to find the 'Unknown Unknowns'.
5. **TestingExcellenceClientSide/flights-client-react** - A browser based UI written using the React framework. This includes component tests written in Jest and Enzyme and also shows support for snapshot files.
6. **TestingExcellenceClientSide/flights-client-angular** - A browser based UI written using the Angular framework. This includes component tests written via the Angular Test Bed and end to end tests written using Protractor.

## Setup

The server-side projects are designed to be imported into IntelliJ. Any recent version of the [IntelliJ Community Edition](https://www.jetbrains.com/idea/download/) should be fine. The projects were built and testing using Java 8, newer versions of Java should work but there could be dependency issues (due to modules being removed from the JDK in versions 9-11). 

You should just need to run the IntelliJ 'Import Project' wizard and select the 'pom.xml', 'build.gradle' or 'build.sbt' file as appropriate. The PBT project requires that your IntelliJ instance also have the Scala plugin installed, but as this project will not be used in exercises it is sufficient to be able to view the files.

The client-side projects are designed to be opened in WebStorm. However any other IDE (such as Visual Studio Code) should be fine also. WebStorm requires a commercial license, but there is a 20 day trial period and we can set you up with a temporary key as long as you are using our LAN.

The following should be noted:

1. Please ensure you are opening the project directory, and not a folder above or below. This will cause IntelliJ / WebStorm to create a new project in that folder, which easily leads to confusion.
2. To run the build files you will need to be able to access the network to download Maven and NPM modules. If you are going to be using your company LAN please make sure you know the appropriate network settings.