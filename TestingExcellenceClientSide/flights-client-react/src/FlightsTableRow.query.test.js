import React from "react";
import {shallow} from "enzyme";
import {FlightsTableRow} from "./FlightsTableRow";

describe("Flight Table Row Component Tests", () => {
    let flag = "";
    const component = shallow(<FlightsTableRow
        selectFlight={(num) => flag = `Select ${num}`}
        updateFlight={(num) => flag = `Update ${num}`}
        deleteFlight={(num) => flag = `Delete ${num}`}
        flight={
            {
                number: "AB12",
                origin: "Belfast",
                destination: "London",
                departure: "01-02-2019",
                arrival: "02-03-2019"
            }
        }/>);
    it("Should render flight details in cells", () => {
        expect(component.find("td").first().text()).toEqual("AB12");
        expect(component.find("td").at(1).text()).toEqual("Belfast");
        expect(component.find("td").at(2).text()).toEqual("01-02-2019");
        expect(component.find("td").at(3).text()).toEqual("London");
        expect(component.find("td").at(4).text()).toEqual("02-03-2019");
    });
    it("Should have a select button triggering handler", () => {
        component.find("td > button").first().simulate("click");
        expect(flag).toEqual("Select AB12");
    });
    it("Should have an update button triggering handler", () => {
        component.find("td > button").at(1).simulate("click");
        expect(flag).toEqual("Update AB12");
    });
    it("Should have a delete button triggering handler", () => {
        component.find("td > button").at(2).simulate("click");
        expect(flag).toEqual("Delete AB12");
    });
});
