import React from "react";
import {shallow} from "enzyme";
import {FlightsTableRow} from "./FlightsTableRow";

describe("Flight Table Row Component Tests", () => {
    let identity = x => x;
    const component = shallow(<FlightsTableRow
        selectFlight={identity}
        updateFlight={identity}
        deleteFlight={identity}
        flight={
            {
                number: "AB12",
                origin: "Belfast",
                destination: "London",
                departure: "01-02-2019",
                arrival: "02-03-2019"
            }
        }/>);
    it("Should render flight details in cells", () => {
        expect(component).toMatchSnapshot();
    });
});
