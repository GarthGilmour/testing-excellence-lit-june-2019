import React from "react"

export const FlightSelected = (props) => {
    if(!props.flight) {
        return null;
    } else {
        let flight = props.flight;
        return (<div className="alert alert-success">
            You have selected flight <span> {flight.number} </span>
            from <span> {flight.origin} </span>
            to <span> {flight.destination} </span>
            leaving on <span> {flight.departure} </span>
            and returning on <span> {flight.arrival} </span>
        </div>);
    }
};
