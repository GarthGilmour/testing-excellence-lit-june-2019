import React from "react";
import {shallow} from "enzyme";
import {FlightSelected} from "./FlightSelected";

describe("Flight Selected Component Tests", () => {
    const component = shallow(<FlightSelected flight={null}/>);
    it("Should render null when flight is null", () => {
        expect(component).toMatchSnapshot();
    });
    it("Should render a div containing text describing the flight", () => {
        component.setProps({
            flight: {
                number: "AB12",
                origin: "Belfast",
                destination: "London",
                departure: "01-02-2019",
                arrival: "02-03-2019"
            }
        });
        expect(component.get(0)).toMatchSnapshot();
    });
});
