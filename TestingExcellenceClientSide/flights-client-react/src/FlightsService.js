import axios from "axios";

class FlightsService {
    constructor() {
        this.baseUrl = 'http://localhost:8080/flights';
    }
    async getAllFlights() {
        const response = await axios.get(`${this.baseUrl}/`);
        return response.data;
    }
    async selectFlight(number) {
        const response = await axios.get(`${this.baseUrl}/${number}`);
        return response.data;
    }
    async deleteFlight(number) {
        const response = await axios.delete(`${this.baseUrl}/${number}`);
        return response.data;
    }
    async updateFlight(flight) {
        let headers = {
            'Accept': 'text/plain',
            'Content-Type': 'application/json'
        };
        const response = await axios.put(
            `${this.baseUrl}/`, flight, {headers});
        return response.data;
    }
}

export default FlightsService;



