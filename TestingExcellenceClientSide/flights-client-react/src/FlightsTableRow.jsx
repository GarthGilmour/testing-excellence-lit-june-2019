import React from 'react';

export const FlightsTableRow = ({flight, selectFlight, updateFlight, deleteFlight}) => (
    <tr>
        <td>{flight.number}</td>
        <td>{flight.origin}</td>
        <td>{flight.departure}</td>
        <td>{flight.destination}</td>
        <td>{flight.arrival}</td>
        <td className="btn-group">
            <button className='btn btn-info'
                    onClick={() => selectFlight(flight.number)}>Select
            </button>
            <button className='btn btn-info'
                    onClick={() => updateFlight(flight.number)}>Update
            </button>
            <button className='btn btn-info'
                    onClick={() => deleteFlight(flight.number)}>Delete
            </button>
        </td>
    </tr>
);
