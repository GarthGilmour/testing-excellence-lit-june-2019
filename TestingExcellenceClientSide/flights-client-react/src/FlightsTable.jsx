import React from 'react';
import {FlightsTableRow} from "./FlightsTableRow";

export const FlightsTable = ({flights, selectFlight, updateFlight, deleteFlight}) => (
    <table className='table table-striped table-hover'>
        <thead>
        <tr>
            <th>Number</th>
            <th>Origin</th>
            <th>Departure</th>
            <th>Destination</th>
            <th>Arrival</th>
            <th/>
        </tr>
        </thead>
        <tbody>
        {flights.map(flight => (
            <FlightsTableRow key={flight.number}
                             flight={flight}
                             selectFlight={selectFlight}
                             updateFlight={updateFlight}
                             deleteFlight={deleteFlight}/>
        ))}
        </tbody>
    </table>
);
