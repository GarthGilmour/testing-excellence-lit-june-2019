import React from "react";
import {shallow} from "enzyme";
import {FlightSelected} from "./FlightSelected";

describe("Flight Selected Component Tests", () => {
    const component = shallow(<FlightSelected flight={null}/>);
    it("Should render null when flight is null", () => {
        expect(component.get(0)).toBeNull();
    });
    it("Should render a div containing text describing the flight", () => {
        component.setProps({
            flight: {
                number: "tst",
                origin: "tst",
                destination: "tst",
                departure: "tst",
                arrival: "tst"
            }
        });
        expect(component.get(0)).not.toBeNull();
        expect(component.find("div").text()).toContain("You have selected flight ");
        expect(component.find("div").text()).toContain(" from ");
        expect(component.find("div").text()).toContain(" to ");
        expect(component.find("div").text()).toContain(" leaving on ");
        expect(component.find("div").text()).toContain(" and returning on ");
    });

    it("Should render flight details in spans", () => {
        component.setProps({
            flight: {
                number: "AB12",
                origin: "Belfast",
                destination: "London",
                departure: "01-02-2019",
                arrival: "02-03-2019"
            }
        });
        expect(component.find("span").first().text()).toEqual(" AB12 ");
        expect(component.find("span").at(1).text()).toEqual(" Belfast ");
        expect(component.find("span").at(2).text()).toEqual(" London ");
        expect(component.find("span").at(3).text()).toEqual(" 01-02-2019 ");
        expect(component.find("span").at(4).text()).toEqual(" 02-03-2019 ");
    });
});
