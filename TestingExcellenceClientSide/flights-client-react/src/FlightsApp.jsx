import React, {Component} from 'react';
import FlightsService from "./FlightsService";
import {FlightsTable} from "./FlightsTable";
import {FlightSelected} from "./FlightSelected";
import {FlightUpdate} from "./FlightUpdate";

export class FlightsApp extends Component {
    constructor(props) {
        super(props);
        this.service = new FlightsService();
        this.state = {
            flights: [],
            selected: null,
            updating: null
        };
    }

    async componentDidMount() {
        await this.refreshFlights();
    }

    refreshFlights = async () => {
        const flights = await this.service.getAllFlights();
        this.setState({
            flights,
            selected: null,
            updating: null
        });
    };

    deleteFlight = async (number) => {
        console.log("Delete called with ", number);
        await this.service.deleteFlight(number);
        await this.refreshFlights();
    };

    selectFlight = async (number) => {
        console.log("Select called with ", number);
        let selected = await this.service.selectFlight(number);
        this.setState({selected});
    };

    updateFlight = async (number) => {
        console.log("Update called with ", number);
        let updating = await this.service.selectFlight(number);
        this.setState({updating});
    };

    finishUpdate = async (flight) => {
        console.log("Finish update called with ", flight);
        await this.service.updateFlight(flight);
        await this.refreshFlights();
    };

    render() {
        return (
            <div className='container'>
                <FlightSelected flight={this.state.selected}/>
                <FlightsTable flights={this.state.flights}
                              selectFlight={this.selectFlight}
                              updateFlight={this.updateFlight}
                              deleteFlight={this.deleteFlight}/>
                {this.state.updating
                    ? <FlightUpdate flight={this.state.updating}
                                  finishUpdate={this.finishUpdate}/>
                    : null
                }
            </div>
        )
    }
}
