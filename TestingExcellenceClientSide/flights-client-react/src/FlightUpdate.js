import React from "react";
import {Component} from "react";

const FlightError = (props) => {
    return <span>
        {props.input === ""
            ? <span className="alert alert-danger">{props.message}</span>
            : null}
    </span>
};

export class FlightUpdate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            flight: props.flight,
            valid: true
        };
        this.finishUpdate = this.finishUpdate.bind(this);
    }

    finishUpdate(event) {
        event.preventDefault();
        if(this.state.valid) {
            this.props.finishUpdate(this.state.flight);
        }
    }

    render() {
        return (<form onSubmit={this.finishUpdate}>
            <h3>Flight Number: {this.state.flight.number}</h3>
            <fieldset className="form-group">
                <label>Flight Origin:</label>
                <input id="origin" size="30" type="text"
                       value={this.state.flight.origin}
                       onChange={e => this.updateProperty("origin", e)}
                       className="form-control"/>
                <FlightError input={this.state.flight.origin}
                             message="The origin airport is required"/>
            </fieldset>
            <fieldset className="form-group">
                <label>Flight Destination:</label>
                <input id="destination" size="30" type="text"
                       value={this.state.flight.destination}
                       onChange={e => this.updateProperty("destination", e)}
                       className="form-control"/>
                <FlightError input={this.state.flight.destination}
                             message="The destination airport is required"/>
            </fieldset>
            <fieldset className="form-group">
                <label>Departure Time:</label>
                <input size="30" type="text"
                       value={this.state.flight.departure}
                       onChange={e => this.updateProperty("departure", e)}
                       className="form-control"/>
                <FlightError input={this.state.flight.departure}
                             message="The departure is required"/>
            </fieldset>
            <fieldset className="form-group">
                <label>Arrival Time:</label>
                <input size="30" type="text"
                       value={this.state.flight.arrival}
                       onChange={e => this.updateProperty("arrival", e)}
                       className="form-control"/>
                <FlightError input={this.state.flight.arrival}
                             message="The arrival time is required"/>
            </fieldset>
            <input id="submit" type="submit" value="Update Flight"/>
        </form>);
    }

    updateProperty(key, event) {
        let value = event.target.value;
        let flight = this.state.flight;
        flight[key] = value;
        this.setState({
            flight,
            valid: value !== ""
        });
    }
}
