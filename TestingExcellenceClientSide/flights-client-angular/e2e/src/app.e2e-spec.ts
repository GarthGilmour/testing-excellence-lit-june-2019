///<reference path="app.po.ts"/>
import {AppPage} from './app.po';

describe('Flight Booking App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();

    page.navigateTo();
  });

  it('should display table with 21 rows', () => {
    expect(page.getFlightsTableRows().count()).toEqual(21);
  });

  it('should display correct details for course 105', async () => {
    const flight = await page.getFlight(105);

    expect(flight.number).toEqual('105');
    expect(flight.origin).toEqual('Belfast City');
    expect(flight.destination).toEqual('London Gatwick');
  });

  it('should show details for course 201', async () => {
    page.selectFlight(201);

    const flightMessage = page.getFlightDetails().getText();

    expect(flightMessage)
      .toMatch('You have selected flight 201 from Belfast City to Birmingham leaving on (.*) and returning on (.*)');
  });

  it('should show update form for course 205', async () => {
    page.updateFlight(205);

    const flight = await page.getFlightFormValue();

    expect(flight.number).toEqual('205');
    expect(flight.origin).toEqual('Belfast City');
    expect(flight.destination).toEqual('Birmingham');
  });

  function updateFlightForm(origin: string, destination: string) {
    page.updateFlightForm(origin, destination);
    page.saveFlightUpdate();
  }

  it('should update flight correctly', async () => {
    page.updateFlight(301);

    updateFlightForm('AAA', 'BBB');

    const updatedFlight = await page.getFlight(301);
    expect(updatedFlight.number).toEqual('301');
    expect(updatedFlight.origin).toEqual('AAA');
    expect(updatedFlight.destination).toEqual('BBB');
  });
});
