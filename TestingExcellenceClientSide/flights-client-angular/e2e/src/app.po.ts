import {browser, by, element, ElementFinder} from 'protractor';

enum Cell {
  Number = 0,
  Origin = 1,
  Destination = 3,
  Select = 5,
  Update = 6
}

const UPDATE_CELL_INDEX = 6;
const NUMBER_CELL_INDEX = 6;

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getFlightsTable() {
    return element(by.id('flights-table'));
  }

  getFlightsTableRows() {
    return this.getFlightsTable().all(by.tagName('tr'));
  }

  getFlightRow(id: number) {
    return element(by.id('flight-row-' + id.toString()));
  }

  async getFlight(flightId: number) {
    const flightRow = this.getFlightRow(flightId);
    return {
      number: await flightRow.element(by.id('number')).getText(),
      origin: await flightRow.element(by.id('origin')).getText(),
      destination: await flightRow.element(by.id('destination')).getText(),
    };
  }

  getFlightDetails() {
    return element(by.id('flight-details'));
  }

  getFlightUpdateForm() {
    return element(by.tagName('app-flight-update'));
  }

  updateFlight(flightId: number) {
    return this.getFlightRow(flightId)
      .element(by.id('update'))
      .click();
  }

  selectFlight(flightId: number) {
    return this.getFlightRow(flightId)
      .element(by.id('select'))
      .click();
  }

  async getFlightFormValue() {
    const flightUpdateForm = this.getFlightUpdateForm();
    return {
      number: await flightUpdateForm.element(by.id('number')).getAttribute('value'),
      origin: await flightUpdateForm.element(by.id('origin')).getAttribute('value'),
      destination: await flightUpdateForm.element(by.id('destination')).getAttribute('value'),
    };
  }

  setText(elementFinder: ElementFinder, text: string) {
    elementFinder.clear();
    elementFinder.sendKeys(text);
  }

  updateFlightForm(origin: string, destination: string) {
    const flightUpdateForm = this.getFlightUpdateForm();

    this.setText(flightUpdateForm.element(by.id('origin')), origin);
    this.setText(flightUpdateForm.element(by.id('destination')), destination);
  }

  saveFlightUpdate() {
    this.getFlightUpdateForm().element(by.id('submit')).click();
  }
}
