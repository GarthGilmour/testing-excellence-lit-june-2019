import {Component, Input, Output, EventEmitter, OnInit, OnChanges} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import 'rxjs/add/operator/filter';

import { Flight } from '../models/flight.model';

@Component({
    selector: 'app-flight-update',
    templateUrl: 'flight-update.html',
    styleUrls: ['flight-update.css']
})
export class FlightUpdateComponent implements OnInit, OnChanges {
    @Input()
    flight: Flight;
    @Output()
    onFlightUpdated = new EventEmitter<Flight>();

    updateForm: FormGroup;
    name = 'FlightUpdateComponent';

    constructor(private formBuilder: FormBuilder) {
        function composeValidators(thePattern: RegExp) {
            const v1 = Validators.required;
            const v2 = Validators.pattern(thePattern);
            return Validators.compose([v1, v2]);
        }
        function crossFieldsValidator(fg: FormGroup) {
            if (fg.get('flightOrigin').value === fg.get('flightDestination').value) {
                return {'identicalAirports': true};
            }
            return null;
        }

        this.updateForm = formBuilder.group({
            flightNumber: ['', composeValidators(/^[0-9]{3}$/)],
            flightOrigin: ['', composeValidators(/^[a-zA-Z ]+$/)],
            flightDestination: ['', composeValidators(/^[a-zA-Z ]+$/)],
            departureTime: ['', Validators.required],
            arrivalTime: ['', Validators.required]
        }, {'validator': crossFieldsValidator});
    }
    ngOnInit() {
        this.resetForm();
        this.updateForm
            .valueChanges
            .filter(() => this.updateForm.valid)
            .subscribe(values => {
                this.resetModel(values);
            });
    }
    ngOnChanges() {
        this.resetForm();
    }
    updateFlight(): void {
        console.log('Update of flight complete');
        this.onFlightUpdated.emit(this.flight);
    }
    private resetForm(): void {
        this.updateForm.setValue({
            flightNumber: this.flight.number,
            flightOrigin: this.flight.origin,
            flightDestination: this.flight.destination,
            departureTime: this.flight.departure,
            arrivalTime: this.flight.arrival
        });
    }
    private resetModel(values: any): void {
        console.log('Updating flight object via Observable');
        this.flight.number = values.flightNumber;
        this.flight.origin = values.flightOrigin;
        this.flight.destination = values.flightDestination;
        this.flight.departure = values.departureTime;
        this.flight.arrival = values.arrivalTime;
    }
}
