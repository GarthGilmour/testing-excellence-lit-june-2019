import {TestBed, async} from '@angular/core/testing';
import {FlightUpdateComponent} from './flight-update.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {Flight} from '../models/flight.model';

describe('FlightSelectedComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule],
      declarations: [
        FlightUpdateComponent
      ],
    }).compileComponents();
  }));
  it('should create the component', async(() => {
    const fixture = TestBed.createComponent(FlightUpdateComponent);
    const comp = fixture.debugElement.componentInstance;

    expect(comp).toBeTruthy();
  }));
  it('should have a name', async(() => {
    const fixture = TestBed.createComponent(FlightUpdateComponent);
    const comp = fixture.debugElement.componentInstance;

    expect(comp.name).toEqual('FlightUpdateComponent');
  }));
  it('should contain a formGroup', async(() => {
    const fixture = TestBed.createComponent(FlightUpdateComponent);
    const comp = fixture.debugElement.componentInstance;
    const controls = comp.updateForm;

    expect(controls.contains('flightNumber')).toBeTruthy();
    expect(controls.contains('flightOrigin')).toBeTruthy();
    expect(controls.contains('flightDestination')).toBeTruthy();
    expect(controls.contains('departureTime')).toBeTruthy();
    expect(controls.contains('arrivalTime')).toBeTruthy();
  }));
  it('should be invalid by default', async(() => {
    const fixture = TestBed.createComponent(FlightUpdateComponent);
    const comp = fixture.debugElement.componentInstance;

    expect(comp.updateForm.valid).toBeFalsy();
  }));
  it('should recognise valid data', async(() => {
    const fixture = TestBed.createComponent(FlightUpdateComponent);
    const comp = fixture.debugElement.componentInstance;

    const data = {
      number: 901,
      origin: 'Belfast',
      destination: 'Dublin',
      departure: '2019-01-01',
      arrival: '2019-01-03'
    };
    comp.flight = new Flight(data);
    fixture.detectChanges();

    expect(comp.updateForm.valid).toBeTruthy();
  }));
  it('should validate flight numbers', async(() => {
    const fixture = TestBed.createComponent(FlightUpdateComponent);
    const comp = fixture.debugElement.componentInstance;

    expect(comp.updateForm.controls.flightNumber.valid).toBeFalsy();

    comp.updateForm.controls.flightNumber.setValue('123');
    expect(comp.updateForm.controls.flightNumber.valid).toBeTruthy();

    comp.updateForm.controls.flightNumber.setValue('1X3');
    expect(comp.updateForm.controls.flightNumber.valid).toBeFalsy();
  }));
  it('should validate flight origin', async(() => {
    const fixture = TestBed.createComponent(FlightUpdateComponent);
    const comp = fixture.debugElement.componentInstance;

    expect(comp.updateForm.controls.flightOrigin.valid).toBeFalsy();
    comp.updateForm.controls.flightOrigin.setValue('Birmingham');

    expect(comp.updateForm.controls.flightOrigin.valid).toBeTruthy();

    comp.updateForm.controls.flightOrigin.setValue('B1rmingham');
    expect(comp.updateForm.controls.flightOrigin.valid).toBeFalsy();
  }));
  it('should validate flight destination', async(() => {
    const fixture = TestBed.createComponent(FlightUpdateComponent);
    const comp = fixture.debugElement.componentInstance;

    expect(comp.updateForm.controls.flightDestination.valid).toBeFalsy();

    comp.updateForm.controls.flightDestination.setValue('London');
    expect(comp.updateForm.controls.flightDestination.valid).toBeTruthy();

    comp.updateForm.controls.flightDestination.setValue('Lon3on');
    expect(comp.updateForm.controls.flightDestination.valid).toBeFalsy();
  }));
});
