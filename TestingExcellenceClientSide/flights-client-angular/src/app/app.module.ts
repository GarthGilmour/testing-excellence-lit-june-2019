import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { FlightViewerComponent } from './flight-viewer/flight-viewer.component';
import { FlightRowComponent } from './flight-row/flight-row.component';
import { FlightSelectedComponent } from './flight-selected/flight-selected.component';
import { FlightUpdateComponent } from './flight-update/flight-update.component';
import { FlightService } from './services/flight.service';
import { FlightServiceHttp } from './services/flight.service.http';

@NgModule({
    imports:      [ BrowserModule,
                    FormsModule,
                    ReactiveFormsModule,
                    HttpClientModule ],
    declarations: [ FlightViewerComponent, FlightRowComponent,
                    FlightSelectedComponent, FlightUpdateComponent ],
    bootstrap:    [ FlightViewerComponent ],
    providers:    [{provide: FlightService, useClass: FlightServiceHttp}]
})
export class AppModule { }
