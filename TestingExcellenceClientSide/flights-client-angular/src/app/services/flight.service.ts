import { Observable } from 'rxjs';
import { Flight } from '../models/flight.model';

export abstract class FlightService {
    abstract fetchFlights(): Observable<Flight[]>;
    abstract updateFlight(flight: Flight): Observable<string>;
}
