import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { FlightService } from './flight.service';
import { Flight } from '../models/flight.model';
import {FlightJson} from '../models/flight.protocol';

@Injectable()
export class FlightServiceHttp extends FlightService {
    baseUrl = 'http://localhost:8080/flights/';
    constructor(private http: HttpClient) {
        super();
    }
    fetchFlights(): Observable<Flight[]> {
        return this.http
                   .get<FlightJson[]>(this.baseUrl)
                   .map(jsonArray => jsonArray.map((item: FlightJson) => new Flight(item)));
    }
    updateFlight(flight: Flight): Observable<string> {
        const url = this.baseUrl;
        const headers = new HttpHeaders({'Content-Type': 'application/json'});
        return this.http
                   .put(url, flight, {headers, responseType: 'text'});
    }

}
