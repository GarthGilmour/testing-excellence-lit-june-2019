import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

import {FlightService} from './flight.service';
import {FlightServiceHttp} from './flight.service.http';
import 'rxjs/add/observable/of';
import {Flight} from '../models/flight.model';
import {HttpClient} from '@angular/common/http';

describe('FlightServiceHttp', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let service: FlightService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {provide: FlightService, useClass: FlightServiceHttp},
      ]
    });
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(FlightService);
  });

  it('should instantiate flight service', () => {
    expect(service).toBeTruthy();
  });

  it('should fetch flights', () => {
    service.fetchFlights().subscribe((flights) => {
      expect(flights.length).toEqual(2);
      expect(flights[0].number).toEqual(901);
      expect(flights[1].number).toEqual(902);
    });
    const request = httpTestingController.expectOne('http://localhost:8080/flights/');
    request.flush([{
      number: 901,
      origin: 'Belfast',
      destination: 'Dublin',
      departure: '2019-01-01',
      arrival: '2019-01-03'
    }, {
      number: 902,
      origin: 'London',
      destination: 'Heathrow',
      departure: '2019-02-02',
      arrival: '2019-02-03'
    }
    ]);
    httpTestingController.verify();
  });
  it('should update flights', () => {
    const data = {
      number: 901,
      origin: 'Atlantis',
      destination: 'Olympus',
      departure: '2019-02-02',
      arrival: '2019-02-03'
    };
    const flight = new Flight(data);
    service.updateFlight(flight).subscribe(retval => {
      expect(retval).toBeTruthy();
    });
    const request = httpTestingController.expectOne('http://localhost:8080/flights/');
    expect(request.request.method).toEqual('PUT');
    expect(request.request.headers.get('Content-Type')).toEqual('application/json');

    request.flush('true');
    httpTestingController.verify();
  });
});
