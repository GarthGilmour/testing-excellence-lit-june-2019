import {TestBed, async} from '@angular/core/testing';
import {FlightSelectedComponent} from './flight-selected.component';
import {Flight} from '../models/flight.model';

describe('FlightSelectedComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        FlightSelectedComponent
      ],
    }).compileComponents();
  }));
  it('should create the component', async(() => {
    const fixture = TestBed.createComponent(FlightSelectedComponent);
    const comp = fixture.debugElement.componentInstance;

    expect(comp).toBeTruthy();
  }));
  it('should have a name', async(() => {
    const fixture = TestBed.createComponent(FlightSelectedComponent);
    const comp = fixture.debugElement.componentInstance;

    expect(comp.name).toEqual('FlightSelectedComponent');
  }));
  it('should NOT render a div without flight', async(() => {
    const fixture = TestBed.createComponent(FlightSelectedComponent);
    const children = fixture.debugElement.children;

    expect(children.length).toEqual(0);
  }));
  it('should render a div with flight', async(() => {
    const fixture = TestBed.createComponent(FlightSelectedComponent);
    const component = fixture.componentInstance;

    const data = {
      number: 901,
      origin: 'Belfast',
      destination: 'Dublin',
      departure: '2019-01-01',
      arrival: '2019-01-03'
    };
    component.flight = new Flight(data);
    fixture.detectChanges();

    const children = fixture.debugElement.children;
    expect(children.length).toEqual(1);

    const parentElement = children[0].nativeElement;
    expect(parentElement.tagName).toEqual('DIV');
  }));
  it('should display flight details', async(() => {
    const fixture = TestBed.createComponent(FlightSelectedComponent);
    const component = fixture.componentInstance;

    const data = {
      number: 901,
      origin: 'Belfast',
      destination: 'Dublin',
      departure: '2019-01-01',
      arrival: '2019-01-03'
    };
    component.flight = new Flight(data);
    fixture.detectChanges();

    const divElement = fixture.debugElement.nativeElement;
    const content = divElement.textContent;

    expect(content).toContain('You have selected flight 901');
    expect(content).toContain('from Belfast');
    expect(content).toContain('to Dublin');
    expect(content).toContain('leaving on Jan 1, 2019');
    expect(content).toContain('and returning on Jan 3, 2019');
  }));
});
