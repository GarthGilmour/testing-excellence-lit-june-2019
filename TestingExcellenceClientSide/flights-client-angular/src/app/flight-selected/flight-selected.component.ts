import { Component, Input } from '@angular/core';

import { Flight } from '../models/flight.model';

@Component({
    selector: 'app-flight-selected',
    templateUrl: 'flight-selected.html',
    styleUrls: ['flight-selected.css']
})
export class FlightSelectedComponent {
    @Input()
    flight: Flight;
    name = 'FlightSelectedComponent';
}
