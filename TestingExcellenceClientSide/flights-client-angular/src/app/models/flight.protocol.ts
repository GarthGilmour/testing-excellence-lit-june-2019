export interface FlightJson {
  number: number;
  origin: string;
  departure: string;
  destination: string;
  arrival: string;
}
