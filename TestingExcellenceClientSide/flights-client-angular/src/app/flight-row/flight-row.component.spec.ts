import {TestBed, async} from '@angular/core/testing';
import {FlightRowComponent} from './flight-row.component';
import {Flight} from '../models/flight.model';

describe('FlightRowComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        FlightRowComponent
      ],
    }).compileComponents();
  }));
  it('should create the component', async(() => {
    const fixture = TestBed.createComponent(FlightRowComponent);
    const comp = fixture.debugElement.componentInstance;

    expect(comp).toBeTruthy();
  }));
  it('should have a name', async(() => {
    const fixture = TestBed.createComponent(FlightRowComponent);
    const comp = fixture.debugElement.componentInstance;

    expect(comp.name).toEqual('FlightRowComponent');
  }));
  it('should render six cells', async(() => {
    const fixture = TestBed.createComponent(FlightRowComponent);
    const rows = fixture.debugElement.children;

    expect(rows.length).toEqual(6);
    for (let x = 0; x < 6; x++) {
      const element = rows[x].nativeElement;
      expect(element.tagName).toEqual('TD');
    }
  }));
  it('should render a select button', async(() => {
    const fixture = TestBed.createComponent(FlightRowComponent);
    const rows = fixture.debugElement.children;
    const element = rows[5].nativeElement;
    const childElement = element.children[0];

    expect(childElement.tagName).toEqual('BUTTON');
    expect(childElement.innerHTML).toEqual('Select');
  }));
  it('should render an update button', async(() => {
    const fixture = TestBed.createComponent(FlightRowComponent);
    const rows = fixture.debugElement.children;
    const element = rows[5].nativeElement;
    const childElement = element.children[1];

    expect(childElement.tagName).toEqual('BUTTON');
    expect(childElement.innerHTML).toEqual('Update');
  }));
  it('should display flight details', async(() => {
    const fixture = TestBed.createComponent(FlightRowComponent);
    const rows = fixture.debugElement.children;
    const component = fixture.componentInstance;

    const data = {
      number: 901,
      origin: 'Belfast',
      destination: 'Dublin',
      departure: '2019-01-01',
      arrival: '2019-01-03'
    };
    component.flight = new Flight(data);
    fixture.detectChanges();

    const row1 = rows[0].nativeElement;
    const row2 = rows[1].nativeElement;
    const row3 = rows[2].nativeElement;
    const row4 = rows[3].nativeElement;
    const row5 = rows[4].nativeElement;

    expect(row1.innerHTML).toEqual('901');
    expect(row2.innerHTML).toEqual('Belfast');
    expect(row3.innerHTML).toEqual('Jan 1, 2019');
    expect(row4.innerHTML).toEqual('Dublin');
    expect(row5.innerHTML).toEqual('Jan 3, 2019');
  }));
});
