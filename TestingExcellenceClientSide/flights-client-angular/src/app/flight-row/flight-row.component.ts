import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Flight } from '../models/flight.model';

@Component({
    selector: '[app-flight-row]',
    templateUrl: 'flight-row.html',
    styleUrls: ['flight-row.css']
})

export class FlightRowComponent {
    @Input()
    flight: Flight;
    @Output()
    onFlightSelected = new EventEmitter<Flight>();
    @Output()
    onFlightUpdating = new EventEmitter<Flight>();
    name = 'FlightRowComponent';

    selectFlight() {
        console.log('Just selected ', this.flight.number, ' for display');
        this.onFlightSelected.emit(this.flight);
    }
    updateFlight() {
        console.log('Just selected ', this.flight.number, ' for update');
        this.onFlightUpdating.emit(this.flight);
    }
}
