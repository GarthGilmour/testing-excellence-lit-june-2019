import { Component } from '@angular/core';

import { FlightService } from '../services/flight.service';
import { Flight } from '../models/flight.model';

@Component({
    selector: 'app-flight-viewer',
    templateUrl: 'flight-viewer.html',
    styleUrls: ['flight-viewer.css']
})
export class FlightViewerComponent  {
    name = 'FlightViewerComponent';
    errorMessage = '';
    stateValid = true;
    flights: Flight[];
    selectedFlight: Flight;
    flightToUpdate: Flight;

    constructor(private service: FlightService) {
        this.selectedFlight = null;
        this.flightToUpdate = null;
        this.fetchFlights();
    }
    flightSelected(selected: Flight) {
        console.log('Setting selected flight to: ', selected.number);
        this.selectedFlight = selected;
    }
    flightUpdating(selected: Flight) {
        console.log('Setting updateable flight to: ', selected.number);
        this.flightToUpdate = selected;
    }
    updateFlight(flight: Flight) {
        const errorMsg = `Could not update flight ${flight.number}`;
        this.service
            .updateFlight(flight)
            .subscribe(() => this.fetchFlights(),
                       () => this.raiseError(errorMsg));
    }
    private fetchFlights() {
        this.selectedFlight = null;
        this.flightToUpdate = null;
        this.service
            .fetchFlights()
            .subscribe(flights => this.flights = flights,
                       () => this.raiseError('No flights found!'));
    }
    private raiseError(text: string): void {
        this.stateValid = false;
        this.errorMessage = text;
    }
}
