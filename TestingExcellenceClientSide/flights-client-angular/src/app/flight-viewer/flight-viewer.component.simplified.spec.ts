import {FlightViewerComponent} from './flight-viewer.component';
import {FlightServiceHttp} from '../services/flight.service.http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {Flight} from '../models/flight.model';
import {HttpClient} from '@angular/common/http';

const stubHttp = <HttpClient>(<any>{get: () => Observable.of([])});

const flightService = new FlightServiceHttp(stubHttp);

describe('FlightViewerComponent Simplified', () => {
  beforeEach(() => {
    spyOn(flightService, 'fetchFlights').and.returnValue(Observable.of<Flight[]>([]));
    spyOn(flightService, 'updateFlight').and.returnValue(Observable.of(''));
  });

  it('Should fetch flights on creation via service',  () => {
    const component = new FlightViewerComponent(flightService);
    expect(flightService.fetchFlights).toHaveBeenCalled();
  });

  it('Should try to update flights via service',  () => {
    const component = new FlightViewerComponent(flightService);
    const data = {
      number: 901,
      origin: 'Belfast',
      destination: 'Dublin',
      departure: '2019-01-01',
      arrival: '2019-01-03'
    };
    component.updateFlight(new Flight(data));
    expect(flightService.updateFlight).toHaveBeenCalled();
    expect(flightService.updateFlight).toHaveBeenCalledWith(new Flight(data));
  });
});

