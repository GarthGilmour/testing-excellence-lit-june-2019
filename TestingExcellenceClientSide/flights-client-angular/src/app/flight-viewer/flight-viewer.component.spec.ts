import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FlightViewerComponent} from './flight-viewer.component';
import {FlightService} from '../services/flight.service';
import {FlightServiceHttp} from '../services/flight.service.http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {Flight} from '../models/flight.model';
import {HttpClient} from '@angular/common/http';

import {Component, Directive, EventEmitter, Input} from '@angular/core';
import createSpyObj = jasmine.createSpyObj;

export function createComponentStub(selector: string,
                                    inputs: string[] = [],
                                    outputs: string[] = []) {
  @Component({
    selector: selector,
    template: '',
    inputs: inputs,
    outputs: outputs
  })
  class StubClassComponent {
    constructor() {
      outputs.forEach(x => {
        this[x] = new EventEmitter<any>();
      });
    }
  }

  return StubClassComponent;
}

export function createDirectiveStub(selector: string, keys: string[] = []) {
  @Directive({
    selector: selector,
    inputs: keys
  })
  class StubClassDirective {
  }

  return StubClassDirective;
}

export function createServiceStub<T>(type: new(...params: any[]) => T) {
  return {
    provide: type,
    useValue: createSpyObj('testService', Object.keys(type.prototype))
  };
}

const stubHttp = <HttpClient>(<any>{get: () => Observable.of([])});

const flightService = new FlightServiceHttp(stubHttp);

describe('FlightViewerComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        FlightViewerComponent,
        createComponentStub('app-flight-update', ['flight'], ['flightUpdated']),
        createComponentStub('[app-flight-row]', ['flight'], ['flightSelected', 'flightUpdating']),
        createComponentStub('app-flight-selected', ['flight'], ['flightSelected'])
      ],
      providers: [
        {provide: FlightService, useValue: flightService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    spyOn(flightService, 'fetchFlights').and.returnValue(Observable.of<Flight[]>([]));
    spyOn(flightService, 'updateFlight').and.returnValue(Observable.of(''));
  });

  it('should create', () => {
    const fixture = TestBed.createComponent(FlightViewerComponent);
    const component = fixture.componentInstance;

    expect(component).toBeTruthy();
  });

  it('Should fetch flights on creation via service', async () => {
    const fixture = TestBed.createComponent(FlightViewerComponent);

    expect(flightService.fetchFlights).toHaveBeenCalled();
  });

  it('Should try to update flights via service', async () => {
    const fixture = TestBed.createComponent(FlightViewerComponent);
    const component = fixture.componentInstance;
    const data = {
      number: 901,
      origin: 'Belfast',
      destination: 'Dublin',
      departure: '2019-01-01',
      arrival: '2019-01-03'
    };
    component.updateFlight(new Flight(data));

    expect(flightService.updateFlight).toHaveBeenCalled();
    expect(flightService.updateFlight).toHaveBeenCalledWith(new Flight(data));
  });
});
